export interface BuscarPersonaResponseModel{
    id:number,
    nombre:string,
    apellido:string,
    dni:string,
    fechaNacimiento:Date
}
