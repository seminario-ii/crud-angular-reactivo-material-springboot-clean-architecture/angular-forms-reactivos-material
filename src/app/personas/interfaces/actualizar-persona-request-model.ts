export interface ActualizarPersonaRequestModel {
    id:number,
    nombre:string,
    apellido:string,
    dni:string,
    fechaNacimiento:Date
}
