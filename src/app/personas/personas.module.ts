import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonasRoutingModule } from './personas-routing.module';
import { ListadoComponent } from './pages/listado/listado.component';
import { RegistrarComponent } from './pages/registrar/registrar.component';
import { MaterialModule } from '../material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModificarComponent } from './pages/modificar/modificar.component';
import {ConfirmDialogComponent} from './pages/confirm-dialog/confirm-dialog.component';


@NgModule({
  declarations: [
    ListadoComponent,
    RegistrarComponent,
    ModificarComponent,
    ConfirmDialogComponent
  ],
  imports: [
    CommonModule,
    PersonasRoutingModule,
    MaterialModule,
    ReactiveFormsModule
  ]
})
export class PersonasModule { }
