import { Injectable } from '@angular/core';
import { enviroment } from 'src/enviroments/enviroment';
import {HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';
import { BuscarPersonaResponseModel } from '../interfaces/buscar-persona-response-model';
import { RegistrarPersonaRequestModel } from '../interfaces/RegistrarPersonaRequestModel.interface';
import { ActualizarPersonaRequestModel } from '../interfaces/actualizar-persona-request-model';

@Injectable({
  providedIn: 'root'
})
export class PersonasService {

  private url:string = enviroment.url;

  constructor(private http:HttpClient) {
  }

  buscarPersonas():Observable<BuscarPersonaResponseModel[]>{
    return this.http.get<BuscarPersonaResponseModel[]>(this.url + '/personas');
  }

  buscarPersona(id:number):Observable<BuscarPersonaResponseModel>{
    return this.http.get<BuscarPersonaResponseModel>(this.url + "/personas/" + id);
  }

  registrarPersona(persona:RegistrarPersonaRequestModel):Observable<number>{
    return this.http.post<number>(this.url + '/personas',persona);
  }

  actualizarPersona(persona:ActualizarPersonaRequestModel):Observable<boolean>{
    return this.http.put<boolean>(this.url + "/personas",persona);
  }

  eliminarPersona(id:number):Observable<boolean>{
    return this.http.delete<boolean>(this.url + "/personas/" + id);
  }



}
