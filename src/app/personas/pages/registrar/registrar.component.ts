import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { PersonasService } from '../../services/personas.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Route, Router } from '@angular/router';
import { RegistrarPersonaRequestModel } from '../../interfaces/RegistrarPersonaRequestModel.interface';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.css']
})
export class RegistrarComponent implements OnInit {

  form!: FormGroup;

  constructor(private http: HttpClient, private personaService: PersonasService, private fb: FormBuilder, private router: Router,private dialogRef: MatDialogRef<RegistrarComponent>,private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      nombre: ['', Validators.required],
      apellido: ['', Validators.required],
      dni: ['', Validators.required],
      fechaNacimiento: [null, Validators.required],
    })
  }

  registrar() {
    const persona: RegistrarPersonaRequestModel = this.form.value;
    console.log(persona);
    this.personaService.registrarPersona(persona).subscribe(() => {
      this.dialogRef.close(true);
      this.openSnackBar("La persona se registro con exito","aceptar");
      this.form.reset();
    },(error)=>{
      this.openSnackBar(error.error,"aceptar");
    })
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action,{
      duration: 3000
    });
  }

}
