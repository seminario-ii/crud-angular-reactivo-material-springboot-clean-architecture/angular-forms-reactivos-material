import { Component, OnInit, ViewChild } from '@angular/core';
import { PersonasService } from '../../services/personas.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { BuscarPersonaResponseModel } from '../../interfaces/buscar-persona-response-model';
import { MatDialog } from '@angular/material/dialog';
import { RegistrarComponent } from '../registrar/registrar.component';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { MatSort, Sort } from '@angular/material/sort';
import { ModificarComponent } from '../modificar/modificar.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import {ConfirmDialogComponent} from 'src/app/personas/pages/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {
  displayedColumns: string[] = ['id', 'nombre', 'apellido', 'dni', 'fechaNacimiento', 'Opciones'];
  personas = new MatTableDataSource();

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private personaService: PersonasService, private router: Router, private dialog: MatDialog, private _liveAnnouncer: LiveAnnouncer,private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.getPersonas();
  }


  ngAfterViewInit() {
    this.personas.paginator = this.paginator;
    this.personas.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.personas.filter = filterValue.trim().toLowerCase();
  }

  announceSortChange(sortState: Sort) {
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }

  getPersonas() {
    this.personaService.buscarPersonas().subscribe(
      (personas: BuscarPersonaResponseModel[]) => {
        this.personas.data = personas;
      }
    );
  }

  registrar() {
    const registrar = this.dialog.open(RegistrarComponent);
    registrar.afterClosed().subscribe(result => {
      if (result) {
        this.getPersonas();
      }
    });
  }

  modificar(id: number) {
    const modificar = this.dialog.open(ModificarComponent);
    modificar.componentInstance.id = id;
    modificar.afterClosed().subscribe(result => {
      if (result) {
        this.getPersonas();
      }
    });
  }


  eliminar(id: number) {
    const confirmar = this.dialog.open(ConfirmDialogComponent, {
      data: { message: '¿Está seguro de que desea eliminar esta persona?' }
    });
  
    confirmar.afterClosed().subscribe(result => {
      if (result) {
        this.personaService.eliminarPersona(id).subscribe(() => {
          this.getPersonas();
          this.openSnackBar('La persona se eliminó con éxito', 'Aceptar');
        }, (error) => {
          this.openSnackBar(error.error, 'Aceptar');
        });
      }
    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action,{
      duration: 3000
    });
  }
}
