import { HttpClient } from '@angular/common/http';
import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PersonasService } from '../../services/personas.service';
import { Router } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BuscarPersonaResponseModel } from '../../interfaces/buscar-persona-response-model';
import { ActualizarPersonaRequestModel } from '../../interfaces/actualizar-persona-request-model';

@Component({
  selector: 'app-modificar',
  templateUrl: './modificar.component.html',
  styleUrls: ['./modificar.component.css']
})
export class ModificarComponent {

  @Input() id!:number;

  form!: FormGroup;

  constructor(private http: HttpClient, private personaService: PersonasService, private fb: FormBuilder, private router: Router,private dialogRef: MatDialogRef<ModificarComponent>,private _snackBar: MatSnackBar) { }

  ngOnInit(): void{
    this.form = this.fb.group({
      id:0,
      nombre: ['', Validators.required],
      apellido: ['', Validators.required],
      dni: ['', Validators.required],
      fechaNacimiento: [null, Validators.required],
    })
    this.personaService.buscarPersona(this.id).subscribe((resp:BuscarPersonaResponseModel)=>{
      this.form.patchValue(resp);
    })
  }

  modificar() {
    const persona: ActualizarPersonaRequestModel = this.form.value;
    console.log(persona);
    this.personaService.actualizarPersona(persona).subscribe(() => {
      this.dialogRef.close(true);
      this.openSnackBar("La persona se modifico con exito","aceptar");
      this.form.reset();
    },(error)=>{
      this.openSnackBar(error.error,"aceptar");
    })
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action,{
      duration: 3000
    });
  }

}
